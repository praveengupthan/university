<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>United University</title>
   <?php include 'headerstyles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
       
    <!-- slider home -->
    <div id="carouselExampleIndicators" class="carousel slide">
        <ol class="carousel-indicators d-none">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active" style="background-image: url('img/slider01.jpg')">
                <div class="carousel-caption wow bounceInUp">
                    <h1 class="cartitle py-1"><span class="d-block primaryspan">A Home for </span><span
                            class="secspan">Entrepreneurs</span></h1>
                    <p>United University is the perfect place to start something. From concept to startup, explore
                        resources to turn imagination into opportunity.</p>
                    <a href="javascript:void(0)">Learn More <span class="icon-long-arrow-right icomoon"></span></a>
                </div>
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('img/slider02.jpg')">
                <div class="carousel-caption wow bounceInUp"">
                    <h1 class="cartitle py-1"><span class="d-block primaryspan">A Home for </span><span
                            class="secspan">Entrepreneurs</span></h1>
                    <p>United University is the perfect place to start something. From concept to startup, explore
                        resources to turn imagination into opportunity.</p>
                    <a href="javascript:void(0)">Learn More <span class="icon-long-arrow-right icomoon"></span></a>
                </div>
            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('img/slider03.jpg')">
                <div class="carousel-caption wow bounceInUp"">
                    <h1 class="cartitle py-1"><span class="d-block primaryspan">A Home for </span><span
                            class="secspan">Entrepreneurs</span></h1>
                    <p>United University is the perfect place to start something. From concept to startup, explore
                        resources to turn imagination into opportunity.</p>
                    <a href="javascript:void(0)">Learn More <span class="icon-long-arrow-right icomoon"></span></a>
                </div>
            </div>
            <!-- Slide four - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('img/slider04.jpg')">
                <div class="carousel-caption wow bounceInUp"">
                    <h1 class="cartitle py-1"><span class="d-block primaryspan">A Home for </span><span
                            class="secspan">Entrepreneurs</span></h1>
                    <p>United University is the perfect place to start something. From concept to startup, explore
                        resources to turn imagination into opportunity.</p>
                    <a href="javascript:void(0)">Learn More <span class="icon-long-arrow-right icomoon"></span></a>
                </div>
            </div>
            <!-- Slide five - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url('img/slider05.jpg')">
                <div class="carousel-caption wow bounceInUp"">
                    <h1 class="cartitle py-1"><span class="d-block primaryspan">A Home for </span><span
                            class="secspan">Entrepreneurs</span></h1>
                    <p>United University is the perfect place to start something. From concept to startup, explore
                        resources to turn imagination into opportunity.</p>
                    <a href="javascript:void(0)">Learn More <span class="icon-long-arrow-right icomoon"></span></a>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev diricon " href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="icon-chevron-left icomoon"></span>
        </a>
        <a class="carousel-control-next diricon " href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="icon-chevron-right icomoon"></span>
        </a>
    </div>
    <!--/ home slider -->

    <!-- slider bottoms -->
    <div class="highlets">
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4 col-sm-4">
                    <div class="d-flex highlet-icon align-self-center">
                        <figure class="align-self-center wow bounceInUp"><span class="icon-mouse icomoon"></span></figure>
                        <article class="pl-4 pt-2 wow bounceInUp">
                            <h2 class="h5">Apply online</h2>
                            <p class="pb-2">Lorem Ipsum is simply dummy text of the and industry.</p>
                            <a href="javascript:void(0)">Learn more....</a>
                        </article>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-4 col-sm-4">
                    <div class="d-flex highlet-icon align-self-center">
                        <figure class="align-self-center wow bounceInUp"><span class="icon-cloud-computing icomoon"></span></figure>
                        <article class="pl-4 pt-2 wow bounceInUp">
                            <h2 class="h5">Prospects</h2>
                            <p class="pb-2">Lorem Ipsum is simply dummy text of the and industry.</p>
                            <a href="javascript:void(0)">Learn more....</a>
                        </article>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-4 col-sm-4">
                    <div class="d-flex highlet-icon align-self-center">
                        <figure class="align-self-center wow bounceInUp"><span class="icon-degree icomoon"></span></figure>
                        <article class="pl-4 pt-2 wow bounceInUp">
                            <h2 class="h5">Certification</h2>
                            <p class="pb-2">Lorem Ipsum is simply dummy text of the and industry.</p>
                            <a href="javascript:void(0)">Learn more....</a>
                        </article>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </div>
    <!--/ slider bottoms -->

    <!-- programs section -->
    <div class="overlapSection programs-home">
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <!-- col-->
                <div class="col-lg-4 col-sm-6 align-self-center">
                    <div class="red-box float-right position-relative wow bounceInUp">
                        <h3>BECOME WHO YOU’RE CALLED TO BE</h3>
                        <h2>Majors, Degrees and Programs</h2>
                        <p>Explore more than 150 programs from the undergraduate to doctoral levels that will challenge
                            your thinking, deepen your faith and prepare you for a life of impact.</p>
                        <a href="javascript:void(0)">View our Latest Courses</a>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-8 col-sm-6">
                    <img src="img/home-programs-img.jpg" alt="" class="img-fluid w-100">
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </div>
    <!--/ programs section -->

    <!-- popular courses -->
    <div class="popularCoursesHome">
        <!-- container -->
        <div class="container">
            <!-- title -->
            <div class="row justify-content-center title-row">
                <div class="col-lg-6  text-center wow bounceInUp">
                    <h3>Popular <span>Courses</span></h3>
                    <p> Fusce id sem at ligula laoreet hendrerit venenatis sed purus. Ut pellentesque maximus lacus,
                        necpharetra augue.</p>
                    <a href="javascript:void(0)">More Courses</a>
                </div>
            </div>
            <!--/ title -->

            <!-- row -->
            <div class="row py-5">

                <!-- col -->
                <div class="col-sm-6">
                    <div class="white-col d-flex wow bounceInUp">
                        <figure>
                            <a href="javavscript:void(0)"><img src="img/course01.jpg" alt=""></a>
                        </figure>
                        <article class="position-relative">
                            <h4 class="h5">Aero Space Engineering</h4>
                            <h6>Technology / Space / Aerospace</h6>
                            <p>Classes started from coming friday(21 jun 2017),total seats 72 and available seats 10</p>
                            <a href="javascript:void(0)">Book Now</a>
                            <a>10 Available</a>
                            <span class="rating position-absolute">4.2</span>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-sm-6">
                    <div class="white-col d-flex wow bounceInUp">
                        <figure>
                            <a href="javavscript:void(0)"><img src="img/course02.jpg" alt=""></a>
                        </figure>
                        <article class="position-relative">
                            <h4 class="h5">Fashion Technology</h4>
                            <h6>Technology / Space / Aerospace</h6>
                            <p>Classes started from coming friday(21 jun 2017),total seats 72 and available seats 10</p>
                            <a href="javascript:void(0)">Book Now</a>
                            <a>10 Available</a>
                            <span class="rating position-absolute">4.2</span>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-sm-6">
                    <div class="white-col d-flex wow bounceInUp">
                        <figure>
                            <a href="javavscript:void(0)"><img src="img/course03.jpg" alt=""></a>
                        </figure>
                        <article class="position-relative">
                            <h4 class="h5">Agricultural Courses</h4>
                            <h6>Technology / Space / Aerospace</h6>
                            <p>Classes started from coming friday(21 jun 2017),total seats 72 and available seats 10</p>
                            <a href="javascript:void(0)">Book Now</a>
                            <a>10 Available</a>
                            <span class="rating position-absolute">4.2</span>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-sm-6">
                    <div class="white-col d-flex wow bounceInUp">
                        <figure>
                            <a href="javavscript:void(0)"><img src="img/course04.jpg" alt=""></a>
                        </figure>
                        <article class="position-relative">
                            <h4 class="h5">Marine Engineering</h4>
                            <h6>Technology / Space / Aerospace</h6>
                            <p>Classes started from coming friday(21 jun 2017),total seats 72 and available seats 10</p>
                            <a href="javascript:void(0)">Book Now</a>
                            <a>10 Available</a>
                            <span class="rating position-absolute">4.2</span>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-sm-6">
                    <div class="white-col d-flex wow bounceInUp">
                        <figure>
                            <a href="javavscript:void(0)"><img src="img/course05.jpg" alt=""></a>
                        </figure>
                        <article class="position-relative">
                            <h4 class="h5">Construction Management</h4>
                            <h6>Technology / Space / Aerospace</h6>
                            <p>Classes started from coming friday(21 jun 2017),total seats 72 and available seats 10</p>
                            <a href="javascript:void(0)">Book Now</a>
                            <a>10 Available</a>
                            <span class="rating position-absolute">4.2</span>
                        </article>
                    </div>
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-sm-6">
                    <div class="white-col d-flex wow bounceInUp">
                        <figure>
                            <a href="javavscript:void(0)"><img src="img/course06.jpg" alt=""></a>
                        </figure>
                        <article class="position-relative">
                            <h4 class="h5">Science & Technology</h4>
                            <h6>Technology / Space / Aerospace</h6>
                            <p>Classes started from coming friday(21 jun 2017),total seats 72 and available seats 10</p>
                            <a href="javascript:void(0)">Book Now</a>
                            <a>10 Available</a>
                            <span class="rating position-absolute">4.2</span>
                        </article>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ popular courses -->

    <!-- great faculties-->
    <div class="great-faculties">
        <!-- container -->
        <div class="container">
            <!-- title row-->
            <div class="row justify-content-center title-row pb-5 wow bounceInUp">
                <div class="col-lg-6 text-center">
                    <h3>Our Great  <span>Faculties</span></h3>
                    <p> Fusce id sem at ligula laoreet hendrerit venenatis sed purus. Ut pellentesque maximus lacus,
                        necpharetra augue.</p>
                    <a href="javascript:void(0)">View All Faculties</a>
                </div>
            </div>
            <!--/ title row-->

            <!-- row team-->
            <div class="row">
                <!-- col -->
                <div class="col-sm-6 col-md-3 col-6 text-center facultycol wow bounceInUp">
                    <figure>
                        <img src="img/fac01.jpg" alt="" title="" class="img-fluid">
                    </figure>
                    <h6>John Vettori</h6>
                    <p>Founder & Principle</p>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-sm-6 col-md-3 col-6 text-center facultycol wow bounceInUp">
                    <figure>
                        <img src="img/fac02.jpg" alt="" title="" class="img-fluid">
                    </figure>
                    <h6>Lucy Martin</h6>
                    <p>Founder & Principle</p>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-sm-6 col-md-3 col-6 text-center facultycol wow bounceInUp">
                    <figure>
                        <img src="img/fac03.jpg" alt="" title="" class="img-fluid">
                    </figure>
                    <h6>Chris Evans</h6>
                    <p>Founder & Principle</p>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-sm-6 col-md-3 col-6 text-center facultycol wow bounceInUp">
                    <figure>
                        <img src="img/fac04.jpg" alt="" title="" class="img-fluid">
                    </figure>
                    <h6>Lucy Martin</h6>
                    <p>Founder & Principle</p>
                </div>
                <!--/ col -->
            </div>
            <!--/ row team -->
        </div>
        <!--/ container -->       
    </div>
    <!--/ great faculties-->

    <!-- annual convcation section -->
    <div class="overlapSection convaction-home">
        <div class="container-fluid">
            <!-- row -->
            <div class="row">
                <!-- col-->
                <div class="col-lg-4 col-sm-6 align-self-center order-last">
                    <div class="red-box float-left position-relative wow bounceInUp">
                        <h3>RECENT CONVOCATION</h3>
                        <h2>Annual Convocation 2018</h2>
                        <p>Consectetur adipisicing elit sed do eiusmod tempor inci labore et dolore amit agna aliqua enimnate minim veniam quis nostrud  ullamco laboris...</p>
                        <a href="javascript:void(0)">Read More</a>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-8 col-sm-6">
                    <img src="img/convaction-home.jpg" alt="" class="img-fluid w-100">
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
    </div>
    <!--/ annual convacation section -->

    <!-- blog -->
    <div class="blog-home py-4">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4 col-sm-4">
                     <!-- title row-->
                    <div class="title-row py-4 wow bounceInUp">                   
                        <h3>Our <span>Blog</span></h3>                        
                        <p>Follow our Latest News <a href="javascript:void(0)" class="sep-link"> More Blogs</a>   </p>                
                    </div>
                    <!--/ title row-->

                    <!-- blog -->
                    <div class="blogcol wow bounceInUp">
                        <figure>
                            <a href="javascript:void(0)"><img src="img/blog01.jpg" alt="" class="img-fluid w-100"></a>
                        </figure>
                        <article class="p-2">
                            <p>August 21, 2019</p>
                            <h5 class="py-2"><a href="javascript:void(0)">E-Learning System For Everything</a></h5>
                            <p>Sed ut perspiciatis unde omnis iste natus erro sit voluiptatem accusantium doloremque ips totam quae.</p>
                        </article>
                        <div class="author d-flex ">
                            <img src="img/authorpic01.jpg" alt="" class="w-100">
                            <span class="align-self-center">John Wakson, Admin</span>
                        </div>
                    </div>
                    <!--/ blog-->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4 col-sm-4">
                    <!-- blog -->
                    <div class="blogcol wow bounceInUp">
                        <figure>
                            <a href="javascript:void(0)"><img src="img/blog02.jpg" alt="" class="img-fluid w-100"></a>
                        </figure>
                        <article class="p-2">
                            <p>August 21, 2019</p>
                            <h5 class="py-2"><a href="javascript:void(0)">Way To Learn Than To Go To School</a></h5>
                            <p>Sed ut perspiciatis unde omnis iste natus erro sit voluiptatem accusantium doloremque ips totam quae.</p>
                        </article>
                        <div class="author d-flex ">
                            <img src="img/authorpic01.jpg" alt="">
                            <span class="align-self-center">John Wakson, Admin</span>
                        </div>
                    </div>
                    <!--/ blog-->
                    <!-- students link-->
                    <figure class="position-relative ourstudents wow bounceInUp">
                        <a href="javascript:void(0)"><img src="img/students-link.jpg" alt="" title="" class="img-fluid w-100"></a>
                        <span class="position-absolute fwhite">Our Students</span>
                    </figure>
                    <!--/ students link-->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-4 col-sm-4">
                     <!-- blog -->
                     <div class="blogcol wow bounceInUp">                        
                        <article class="p-2">
                            <p>August 21, 2019</p>
                            <h5 class="py-2"><a href="javascript:void(0)">The benefits of Team work</a></h5>
                            <p>Sed ut perspiciatis unde omnis iste natus erro sit voluiptatem accusantium doloremque ips totam quae.</p>
                        </article>
                        <div class="author d-flex ">
                            <img src="img/authorpic01.jpg" alt="">
                            <span class="align-self-center">John Wakson, Admin</span>
                        </div>
                    </div>
                    <!--/ blog-->
                    <!-- registration-->
                    <figure class="position-relative ourstudents mt-4 wow bounceInUp">
                        <a href="javascript:void(0)"><img src="img/home-register-img.jpg" alt="" title="" class="img-fluid w-100"></a>
                        <article class="position-absolute">
                            <span>Course Registration</span>
                            <P>Sed ut perspiciatis unde omnis iste natus erro sit voluiptatem </P>
                        </article>
                    </figure>
                    <!--/ registration-->
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container-->
    </div>
    <!--/ blog -->

    <!-- photo gallery -->
    <div class="home-gallery pt-5">
        <!-- container -->
        <div class="container">
            <!-- title row-->
            <div class="row justify-content-center title-row pb-5 wow bounceInUp">
                <div class="col-lg-6 text-center">
                    <h3>Photo <span>Gallery</span></h3>
                    <p> See and Feel it</p>                   
                </div>
            </div>
            <!--/ title row-->
        </div>
        <!-- container -->

        <div class="gallery-block grid-gallery">
            <div class="container-fluid">              
                <div class="row">

                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image1.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image1.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image2.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image2.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image3.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image3.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image4.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image4.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image5.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image5.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image6.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image6.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image7.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image7.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image8.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image8.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image9.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image9.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 
                    col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image10.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image10.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image11.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image11.jpg">
                        </a>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2 item wow bounceInUp">
                        <a class="lightbox" href="img/gallery/image12.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/gallery/image12.jpg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ photo gallery-->

    <!-- students testimonails-->
    <div class="student-testimonials">
        <!-- container-->
        <div class="container">
            <!-- row -->
            <div class="row justify-content-center wow bounceInUp">
                <div class="col-lg-6 text-center">
                    <!-- slider start -->
                    <div id="cartesti" class="carousel slide">                            
                            <div class="carousel-inner" role="listbox">
                                <!-- Slide One - Set the background image for this slide in the line below -->
                                <div class="carousel-item active">
                                    <span class="icon-quote-left icomoon"></span>
                                    <h5 class="h4">Stela Hockins</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, </p>
                                    <img class="st-img" src="img/student01.jpg" alt="">
                                    <p><span>Student / Business Studies (department)</span></p>
                                </div>
                                <!-- Slide Two - Set the background image for this slide in the line below -->
                                <div class="carousel-item">
                                    <span class="icon-quote-left icomoon"></span>
                                    <h5 class="h4">Stela Hockins</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, </p>
                                    <img class="st-img" src="img/student02.jpg" alt="">
                                    <p><span>Student / Business Studies (department)</span></p>
                                </div>
                                <!-- Slide Three - Set the background image for this slide in the line below -->
                                <div class="carousel-item">
                                    <span class="icon-quote-left icomoon"></span>
                                    <h5 class="h4">Stela Hockins</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, </p>
                                    <img class="st-img" src="img/student03.jpg" alt="">
                                    <p><span>Student / Business Studies (department)</span></p>
                                </div>
                                <!-- Slide four - Set the background image for this slide in the line below -->
                                <div class="carousel-item">
                                    <span class="icon-quote-left icomoon"></span>
                                    <h5 class="h4">Stela Hockins</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, </p>
                                    <img class="st-img" src="img/student04.jpg" alt="">
                                    <p><span>Student / Business Studies (department)</span></p>
                                </div>
                                <!-- Slide five - Set the background image for this slide in the line below -->
                                <div class="carousel-item">
                                    <span class="icon-quote-left icomoon"></span>
                                    <h5 class="h4">Stela Hockins</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, </p>
                                    <img class="st-img" src="img/student05.jpg" alt="">
                                    <p><span>Student / Business Studies (department)</span></p>
                                </div>
                            </div>
                            <a class="carousel-control-prev diricon " href="#cartesti" role="button" data-slide="prev">
                                <span class="icon-chevron-left icomoon"></span>
                            </a>
                            <a class="carousel-control-next diricon " href="#cartesti" role="button" data-slide="next">
                                <span class="icon-chevron-right icomoon"></span>
                            </a>
                        </div>
                    <!--/ slider ends -->
                </div>
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ students testimonials -->

    <!-- subscribe news letter -->
    <div class="subscribe wow bounceInUp">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-lg-5 align-self-center">
                    <h4 class="h4">SUBSCRIBE TO OUR NEWSLETTER</h4>
                </div>
                <div class="col-lg-6">
                    <input type="text" placeholder="Enter Your Email Address" class="pl-3">
                    <button>Subscribe Now</button>
                </div>
            </div>
        </div>
    </div>
    <!--/ subscribe news letter -->
    <!--/ main -->

    <?php include 'footer.php'?>
</body>

</html>
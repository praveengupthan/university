<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>United University</title>
   <?php include 'headerstyles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>

    <!-- main -->
    <!-- sub page header -->
    <div class="subpage-header">
         <!-- container -->
         <div class="container">
             <div class="row">
                 <div class="col-lg-5">
                    <article class="title-box wow bounceInUp">
                        <h1 class="h1">About <span>United University</span></h1>
                        <p>United University  is the perfect place to start something. From concept to startup, explore resources to turn imagination into opportunity.</p>
                    </article>
                 </div>
             </div>
         </div>
         <!--/ container -->
    </div>
    <!--/ sub page header -->
    <!-- sub page main -->
    <div class="subpage-main">
        <!-- container -->
        <div class="container main-container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-12">
                    <!-- brudcrumb-->
                    <nav>
                        <ol class="breadcrumb mb-0 wow bounceInUp">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>                       
                            <li class="breadcrumb-item active" aria-current="page"> About United University</li>
                        </ol>
                    </nav>
                    <!--/ brudcrumb-->

                    <!-- sub pagein-->
                    <div clas="subpage-in">
                       <!-- row -->
                       <div class="row">
                           <!-- left col -->
                           <div class="col-lg-3 left-col wow bounceInUp">
                               <h2 class="h2">About University</h2>
                               <ul>
                                   <li><a href="javascript:void(0)">University at Glance</a></li>
                                   <li><a href="javascript:void(0)">The Value of University</a></li>
                                   <li><a href="javascript:void(0)">History </a></li>
                                   <li><a href="javascript:void(0)">Administration</a></li>
                                   <li><a href="javascript:void(0)">Directions and Maps</a></li>
                                   <li><a href="javascript:void(0)">Civic Engagement</a></li>
                                   <li><a href="javascript:void(0)">News</a></li>
                               </ul>
                           </div>
                           <!--/ left col -->
                           <!-- right col -->
                           <div class="col-lg-9 right-col wow bounceInUp">
                                <h3 class="h3">University at Glance</h3>
                                <h4 class="h4 py-2">Global Research University, Experintial Learning Leader</h4>
                                <p>United University is a comprehensive global research university with a unique model of experiential learning that combines academic rigor with one of the nation’s premier cooperative education programs.</p>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtin didunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtin didunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtin didunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtin didunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex.</p>

                                <h4 class="h4 py-2 ">Know about Education Park in United Kingdom</h4>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqs U enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irur dolor in reprehenderit in voluptate velit esse cillum dolore eu.</p>

                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqs U enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.</p>


                           </div>
                           <!--/ rightr col -->
                       </div>
                       <!--/ row --> 
                    </div>
                    <!--- sub page in-->
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!--/ sub page main -->
    <!--/ main -->

    <?php include 'footer.php'?>
</body>

</html>
 <link rel="icon" type="image/png" sizes="32x32" href="img/favicon.png">
    <!-- style sheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font.css">
    <!-- gallery -->
    <link rel="stylesheert" href="css/grid-gallery.css">
    <link rel="stylesheet" href="css/baguetteBox.css">
    <link rel="stylesheet" href="css/animation.css">
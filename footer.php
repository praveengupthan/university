<!--/ footer -->
    <footer>
        <!-- footer navigation-->
        <div class="footer-nav">
            <div class="container">
                <ul class="nav">
                    <li class="nav-item"><span>Quick Links:</span></li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.php">About </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Admissions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Courses</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Campus </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Faculty </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Alumni</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">News / Media</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
        <!--/ footer navigation -->

        <!-- bottom footer -->
        <div class="bottom-footer position-relative">
            <!-- container -->
            <div class="container">
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-3 col-sm-3 footerbrand">
                        <a href="javascript:void(0)"><img src="img/logo.svg" alt=""></a>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-5 col-sm-6 footer-social">
                        <div>
                            <a href="javascript:void(0)">
                                <span class="icon-facebook icomoon"></span>
                                <span>Facebook</span>
                            </a>
                            <a href="javascript:void(0)">
                                <span class="icon-twitter icomoon"></span>
                                <span>Twitter</span>
                            </a>
                            <a href="javascript:void(0)">
                                <span class="icon-linkedin icomoon"></span>
                                <span>Linkedin</span>
                            </a>
                        </div>

                        <div>                       
                            <a href="javascript:void(0)">
                                <span class="icon-youtube-play icomoon"></span>
                                <span>Youtube</span>
                            </a>
                            <a href="javascript:void(0)">
                                <span class="icon-instagram icomoon"></span>
                                <span>Instagram</span>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-4 col-sm-6 address align-self-center">
                        <p>© 2019 University of United Kingdom</p>
                        <p>Notre Dame, IN 46556 UK, 41.703234 -86.238985</p>
                        <p><span>Phone:</span> (574) 631-5000</p>
                        <p><span>mail:</span> info@uniteduniversity.com</p>
                    </div>
                    <!--/ col -->
                </div>
            </div>
            <!--/ container -->
            <a href="javascript:void(0)" id="movetop"><span class="icon-arrow-up icomoon"></span></a>
        </div>
        <!--/ bottom footer -->
       
    </footer>
    <!--/ footer -->

    <!-- footer scripts -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/wow.js"></script>
    <script>
        new WOW().init();
    </script>
    <script src="js/custom.js"></script>
    <!--[if lt IE 9]>
        <script src="js/html5-shiv.js"></script>
    <![end if]-->    
    <script src="js/baguetteBox.js"></script>
    <script>
        baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>

    <!--/ footer scripts -->
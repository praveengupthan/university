<!-- header -->
    <header class="fixed-top">
        <!-- top header -->
        <div class="top-header py-1">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 col-sm-6 col-md-6 align-self-center d-none d-sm-block">
                        <p class="headercontact">
                            <span>+(1) 1234 567 890 </span>
                            <span>info@uniteduniversity.com </span>
                        </p>
                    </div>
                    <!--/ col -->

                    <!-- col -->

                    <div class="col-lg-6 col-sm-6 col-md-6 align-self-center">
                        <div class="search-header float-right text-right">
                            <input type="text" class="form-control" placeholder="Search your key words">
                            <button><span class="icon-search icomoon"></span></button>

                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ top header -->

        <!-- navigartion -->
        <div class="container nav-container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="index.php"> <img src="img/logo.svg" alt=""> </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="about.php">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Admissions</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Courses</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Campus</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Faculty</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Alumni</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Media</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
        <!--/ navigation -->
    </header>
    <!--/ header -->